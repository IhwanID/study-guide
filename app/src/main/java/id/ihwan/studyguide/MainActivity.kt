package id.ihwan.studyguide

import android.app.NotificationChannel
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.google.android.material.snackbar.Snackbar
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.Gravity
import java.util.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        findViewById<Button>(R.id.button_snackbar).setOnClickListener {
            showSnackBar(it)
        }

        findViewById<Button>(R.id.button_toast).setOnClickListener {
            showToast()
        }

        findViewById<Button>(R.id.button_notification).setOnClickListener {
            showNotification()
        }

    }

    /**
     * #1 Toast
     */
    private fun showToast(){
        val text = "Hello toast!"
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(applicationContext, text, duration)
        toast.setGravity(Gravity.TOP or Gravity.CENTER, 0, 0)
        toast.show()
    }

    /**
     * #2 Snackbar
     */
    private fun showSnackBar(view: View){
        val snack = Snackbar.make(view,"This is a simple Snackbar",Snackbar.LENGTH_LONG)
        snack.show()
    }

    /**
     * #3 Notification
     */

    private fun showNotification(){
        val manager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = "msg_channel"
        val channelName = "msg_name"
        val id = (Date().time / 1000L) % Integer.MAX_VALUE

        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        notificationIntent.putExtra("message", "This is a notification message")

        val pendingIntent = PendingIntent.getActivity(
            this, 0, notificationIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }

        val builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentTitle("Hello Ihwan")
            .setContentText("I Love You 3000")
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText("Luka luka luka luka yang kau berikan . . ."))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)

        manager.notify(id.toInt(), builder.build())
    }
}
